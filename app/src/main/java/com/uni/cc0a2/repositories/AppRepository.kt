package com.uni.cc0a2.repositories

import com.uni.cc0a2.database.AppDatabase
import com.uni.cc0a2.database.entities.DoctorEntity
import com.uni.cc0a2.database.entities.SpecialtyEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber

class AppRepository(private val database: AppDatabase) {

    private val specialties = listOf(
        SpecialtyEntity("011", "ALERGIA"),
        SpecialtyEntity("012", "ALERGIA INFANTIL"),
        SpecialtyEntity("013", "INMUNOLOGIA"),
        SpecialtyEntity("014", "INMUNOLOGIA INFANTIL"),
        SpecialtyEntity("021", "ANESTESIOLOGIA"),
        SpecialtyEntity("031", "CARDIOANGIOLOGIA"),
        SpecialtyEntity("032", "CARDIOLOGIA"),
        SpecialtyEntity("033", "CARDIOLOGIA GERIATRICA"),
        SpecialtyEntity("034", "CARDIOLOGIA PEDIATRICA"),
        SpecialtyEntity("035", "HEMODINAMIA"),
        SpecialtyEntity("041", "CIRUGIA CARDIOVASCULAR"),
        SpecialtyEntity("042", "CIRUGIA DE CABEZA/CUELLO"),
        SpecialtyEntity("043", "CIRUGIA GASTROENTEROLOG"),
        SpecialtyEntity("044", "CIRUGIA GENERAL"),
        SpecialtyEntity("045", "CIRUGIA INFANTIL"),
        SpecialtyEntity("046", "CIRUGIA ONCOLOGICA"),
        SpecialtyEntity("047", "CIRUGIA PLASTICA"),
        SpecialtyEntity("048", "CIRUGIA PROCTOLOGICA"),
        SpecialtyEntity("049", "CIRUGIA TORAXICA"),
        SpecialtyEntity("040", "CIRUGIA VASCULAR"),
        SpecialtyEntity("051", "CLINICA MEDICA"),
        SpecialtyEntity("061", "MEDICINA DEL DEPORTE"),
        SpecialtyEntity("071", "DERMATOLOGIA"),
        SpecialtyEntity("072", "DERMATOLOGIA INFANTIL"),
        SpecialtyEntity("081", "ENDOCRINOLOGIA"),
        SpecialtyEntity("082", "ENDOCRINOLOGIA INFANTIL"),
        SpecialtyEntity("083", "ENDOCRINOLOGIA/NUTICION"),
        SpecialtyEntity("091", "ANDROLOGIA Y EST MASC"),
        SpecialtyEntity("092", "ESTERILIDAD FEMENINA"),
        SpecialtyEntity("093", "REPRODUCCION"),
        SpecialtyEntity("101", "FLEBOLOGIA"),
        SpecialtyEntity("102", "LINFOLOGIA"),
        SpecialtyEntity("111", "GASTROENTEROLOGIA"),
        SpecialtyEntity("112", "GASTROENTEROLOGIA INFANTIL"),
        SpecialtyEntity("113", "HEPATOLOGIA"),
        SpecialtyEntity("121", "GENETICA"),
        SpecialtyEntity("131", "GERIATRIA Y GERONTOLOGIA"),
        SpecialtyEntity("141", "GINECOLOGIA"),
        SpecialtyEntity("142", "GINECOLOGIA INFANTO-JUVENIL"),
        SpecialtyEntity("143", "OBSTETRICIA"),
        SpecialtyEntity("144", "TOCOGINECOLOGIA"),
        SpecialtyEntity("151", "HEMATOLOGIA"),
        SpecialtyEntity("152", "HEMATOLOGIA INFANTIL"),
        SpecialtyEntity("153", "HEMATOLOGIA ONCOLOGICA"),
        SpecialtyEntity("154", "HEMOSTASIA"),
        SpecialtyEntity("155", "HEMOTERAPIA"),
        SpecialtyEntity("161", "INFECTOLOGIA"),
        SpecialtyEntity("162", "INFECTOLOGIA PEDIATRICA"),
        SpecialtyEntity("163", "PARASITOLOGIA"),
        SpecialtyEntity("171", "FORENSE"),
        SpecialtyEntity("172", "MEDICINA DEL TRABAJO / LABORAL"),
        SpecialtyEntity("173", "MEDICINA LEGAL"),
        SpecialtyEntity("181", "NEFROLOGIA"),
        SpecialtyEntity("182", "NEFROLOGIA INFANTIL"),
        SpecialtyEntity("191", "NEUMONOLOGIA"),
        SpecialtyEntity("192", "NEUMONOLOGIA INFANTIL"),
        SpecialtyEntity("193", "NEUMOTISIOLOGIA"),
        SpecialtyEntity("194", "NEUMOTISIOLOGIA INFANTIL"),
        SpecialtyEntity("201", "NEUROCIRUGIA"),
        SpecialtyEntity("202", "NEUROFISIOLOGIA"),
        SpecialtyEntity("203", "NEUROFONIATRIA"),
        SpecialtyEntity("204", "NEUROLOGIA"),
        SpecialtyEntity("205", "NEUROLOGIA INFANTIL"),
        SpecialtyEntity("211", "DIABETES"),
        SpecialtyEntity("212", "NUTRICION"),
        SpecialtyEntity("213", "NUTRICION INFANTIL"),
        SpecialtyEntity("214", "OBESIDAD"),
        SpecialtyEntity("223", "CLINICA ESTOMATOLOGICA"),
        SpecialtyEntity("231", "OFTALMOLOGIA"),
        SpecialtyEntity("232", "OFTALMOLOGIA INFANTIL"),
        SpecialtyEntity("241", "ONCOLOGIA"),
        SpecialtyEntity("242", "ONCOLOGIA INFANTIL"),
        SpecialtyEntity("251", "OTORRINOLARINGOLOGIA"),
        SpecialtyEntity("252", "OTORRINOLARINGOLOGIA INFANTIL"),
        SpecialtyEntity("261", "ANATOMIA PATOLOGICA"),
        SpecialtyEntity("262", "CITOLOGIA"),
        SpecialtyEntity("271", "NEONATOLOGIA"),
        SpecialtyEntity("272", "PEDIATRIA"),
        SpecialtyEntity("273", "PERINATOLOGIA"),
        SpecialtyEntity("281", "NEUROPSIQUIATRIA"),
        SpecialtyEntity("282", "PSIQUIATRIA"),
        SpecialtyEntity("283", "PSIQUIATRIA INFANTO JUVENIL"),
        SpecialtyEntity("291", "DIAGNOSTICO POR IMAGENES"),
        SpecialtyEntity("292", "ECOGRAFIA"),
        SpecialtyEntity("293", "ELECTROMIOGRAFIA"),
        SpecialtyEntity("294", "MEDICINA NUCLEAR"),
        SpecialtyEntity("295", "RADIOLOGIA"),
        SpecialtyEntity("296", "RADIOTERAPIA"),
        SpecialtyEntity("301", "REUMATOLOGIA"),
        SpecialtyEntity("311", "TOXICOLOGIA"),
        SpecialtyEntity("322", "ORTOPEDIA"),
        SpecialtyEntity("323", "ORTOPEDIA INFANTIL"),
        SpecialtyEntity("324", "OSTEOLOGIA"),
        SpecialtyEntity("325", "TRAUMATOLOGIA Y ORTOPEDIA"),
        SpecialtyEntity("326", "TRAUMATOLOGIA INFANTIL"),
        SpecialtyEntity("331", "UROLOGIA"),
        SpecialtyEntity("332", "UROLOGIA INFANTIL"),
        SpecialtyEntity("991", "VARIOS / OTROS"),
        SpecialtyEntity("221", "CIRUGIA BUCOMAXILOFACIAL"),
        SpecialtyEntity("222", "ENDODONCIA"),
        SpecialtyEntity("224", "ODONTOLOGIA"),
        SpecialtyEntity("225", "ODONTOPEDIATRIA"),
        SpecialtyEntity("226", "ORTODONCIA"),
        SpecialtyEntity("227", "ORTOPEDIA MAXILAR"),
        SpecialtyEntity("228", "PERIODONCIA"),
        SpecialtyEntity("229", "ANATOMIA PATOLOGICA BUCALMAXILOFACIAL"),
        SpecialtyEntity("327", "TRAUMATOLOGIA MAXILO FACIAL"),
        SpecialtyEntity("341", "DIAGNOSTICO POR IMAGENES BUCOMAXILOFACIAL"),
        SpecialtyEntity("342", "PROTESIS DENTOBUCOMAXILAR"),
        SpecialtyEntity("321", "FISIATRIA (MEDICINA FISICA Y REHABILITACION)"),
        SpecialtyEntity("343", "KINESIOLOGIA GENERAL"),
        SpecialtyEntity("344", "KINESIOLOGIA EN PEDIATRIA"),
        SpecialtyEntity("345", "KINESIOLOGIA EN CARDIO RESPIRATORIO"),
        SpecialtyEntity("346", "KINESIOLOGIA EN TRAUMATOLOGIA Y ORTOPEDIA"),
        SpecialtyEntity("47", "PSICOLOGIA CLINICA"),
        SpecialtyEntity("348", "PSICOLOGIA SOCIAL Y/O COMUNITARIA"),
        SpecialtyEntity("349", "PSICOLOGIA JURIDICA Y/O FORENSE"),
        SpecialtyEntity("350", "PSICOLOGIA LABORAL Y/O DEL TRABAJO Y/O DE LAS ORGANIZACIONES"),
        SpecialtyEntity("351", "PSICOLOGIA EDUCACIONAL"),
        SpecialtyEntity("352", "PSICOLOGIA SANITARIA"),
        SpecialtyEntity("353", "BIOQUIMICA"),
        SpecialtyEntity("354", "BROMATOLOGIA, NUTRICION Y TECNOLOGIA DE ALIMENTOS"),
        SpecialtyEntity("355", "BIOTECNOLOGIA"),
    )

    private val doctors = listOf<DoctorEntity>()

    suspend fun populateSpecialtyTable() {
        Timber.i("Populating specialties table ...")
        withContext(Dispatchers.IO) {
            specialties.forEach {
                database.specialtyDao.insert(it)
            }
        }
    }

    suspend fun populateDoctorTable() {
        Timber.i("Populating doctors table ...")
        withContext(Dispatchers.IO) {
            doctors.forEach {
                database.doctorDao.insert(it)
            }
        }
    }
}