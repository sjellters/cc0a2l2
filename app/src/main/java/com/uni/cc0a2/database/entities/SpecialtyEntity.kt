package com.uni.cc0a2.database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "specialty")
data class SpecialtyEntity(
    @PrimaryKey
    val code: String,
    val description: String
)