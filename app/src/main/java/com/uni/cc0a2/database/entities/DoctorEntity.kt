package com.uni.cc0a2.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import java.util.*

@Entity(
    tableName = "doctor", foreignKeys = [
        ForeignKey(
            entity = SpecialtyEntity::class,
            parentColumns = arrayOf("code"),
            childColumns = arrayOf("specialty"),
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class DoctorEntity(
    @PrimaryKey
    val id: String,
    @ColumnInfo(name = "first_name")
    val firstName: String,
    @ColumnInfo(name = "last_name")
    val lastName: String,
    @ColumnInfo(index = true)
    val specialty: String,
    val address: String,
    @ColumnInfo(name = "document_number")
    val documentNumber: String,
    @ColumnInfo(name = "document_type")
    val documentType: String,
    @ColumnInfo(name = "marital_status")
    val maritalStatus: String,
    val gender: String,
    val age: Int,
    @ColumnInfo(name = "date_of_birth")
    val dateOfBirth: Date?,
    val email: String
)