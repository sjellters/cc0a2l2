package com.uni.cc0a2.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import java.util.*

@Entity(
    tableName = "diagnostic", foreignKeys = [
        ForeignKey(
            entity = DoctorEntity::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("patientId"),
            onDelete = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = PatientEntity::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("doctorId"),
            onDelete = ForeignKey.CASCADE
        ),
    ]
)
data class DiagnosticEntity(
    @PrimaryKey
    val id: String,
    @ColumnInfo(index = true)
    val patientId: String,
    @ColumnInfo(index = true)
    val doctorId: String,
    @ColumnInfo(name = "creation_date")
    val creationDate: Date?,
    val status: String,
    val symptoms: String,
    val diagnostic: String,
    val treatment: String
)