package com.uni.cc0a2.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "patient")
data class PatientEntity(
    @PrimaryKey
    val id: String,
    @ColumnInfo(name = "first_name")
    val firstName: String,
    @ColumnInfo(name = "last_name")
    val lastName: String,
    val occupation: String,
    val address: String,
    @ColumnInfo(name = "document_number")
    val documentNumber: String,
    @ColumnInfo(name = "document_type")
    val documentType: String,
    @ColumnInfo(name = "marital_status")
    val civilStatus: String,
    val gender: String,
    val age: Int,
    @ColumnInfo(name = "date_of_birth")
    val dateOfBirth: Date?,
    val email: String,
    val bloodType: String,
    val emergencyPhone: String,
    val insured: Boolean,
    val patientNotes: String
)