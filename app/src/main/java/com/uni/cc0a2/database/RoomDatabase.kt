package com.uni.cc0a2.database

import android.content.Context
import androidx.room.*
import com.uni.cc0a2.database.entities.DiagnosticEntity
import com.uni.cc0a2.database.entities.DoctorEntity
import com.uni.cc0a2.database.entities.PatientEntity
import com.uni.cc0a2.database.entities.SpecialtyEntity
import timber.log.Timber
import java.util.*

class Converters {
    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return value?.let { Date(it) }
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time
    }
}

@Dao
interface SpecialtyDao {

    @Query("SELECT * FROM specialty")
    fun getAll(): List<SpecialtyEntity>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(vararg specialty: SpecialtyEntity)
}

@Dao
interface DiagnosticDao {
    @Query("SELECT * FROM diagnostic")
    fun getAll(): List<DiagnosticEntity>

    @Insert
    suspend fun insert(vararg diagnostic: DiagnosticEntity)
}

@Dao
interface DoctorDao {
    @Query("SELECT * FROM doctor")
    fun getAll(): List<DoctorEntity>

    @Insert
    suspend fun insert(vararg doctor: DoctorEntity)
}

@Dao
interface PatientDao {
    @Query("SELECT * FROM patient")
    fun getAll(): List<PatientEntity>

    @Insert
    suspend fun insert(vararg patient: PatientEntity)
}

private lateinit var INSTANCE: AppDatabase

@Database(
    entities = [
        SpecialtyEntity::class,
        DoctorEntity::class,
        PatientEntity::class,
        DiagnosticEntity::class
    ],
    version = 1
)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract val specialtyDao: SpecialtyDao
    abstract val diagnosticDao: DiagnosticDao
    abstract val doctorDao: DoctorDao
    abstract val patientDao: PatientDao
}

fun getDatabase(context: Context): AppDatabase {
    synchronized(AppDatabase::class.java) {
        if (!::INSTANCE.isInitialized) {
            INSTANCE = Room.databaseBuilder(
                context.applicationContext,
                AppDatabase::class.java,
                "cc0a2l2_db"
            )
                .fallbackToDestructiveMigration()
                .build()
            Timber.i("Creating new database instance")
        } else {
            Timber.i(
                "An instance of the database already exists, returning existing instance"
            )
        }
    }

    return INSTANCE
}
