package com.uni.cc0a2.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.uni.cc0a2.database.AppDatabase
import com.uni.cc0a2.database.getDatabase
import com.uni.cc0a2.repositories.AppRepository

open class ParentViewModel(application: Application) : AndroidViewModel(application) {

    private val database: AppDatabase = getDatabase(application)
    val repository: AppRepository = AppRepository(database)
}