package com.uni.cc0a2.viewmodels

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class HomeViewModel(application: Application, private val sharedViewModel: SharedViewModel) :
    ParentViewModel(application) {

    val documentTypes = listOf("L.E / DNI", "CARNET EXT", "RUC", "PASAPORTE", "P. NAC", "OTROS")

    class Factory(
        private val application: Application,
        private val sharedViewModel: SharedViewModel
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(HomeViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return HomeViewModel(application, sharedViewModel) as T
            }
            throw IllegalArgumentException("Unable to construct view model")
        }
    }
}