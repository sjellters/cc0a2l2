package com.uni.cc0a2.viewmodels

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.uni.cc0a2.database.entities.PatientEntity
import java.util.*

class PatientDataViewModel(application: Application, private val sharedViewModel: SharedViewModel) :
    ParentViewModel(application) {

    val currentPatient = PatientEntity(
        "1",
        "Diego",
        "Andrade",
        "Desarrollador de software",
        "Psje. Los Florales 173, Urb. El Progreso - Carabayllo",
        "75545252",
        "L.E / DNI",
        "Soltero",
        "Masculino",
        22,
        Date(),
        "andradec.diego@gmail.com",
        "B+",
        "+51926451081",
        true,
        "Este paciente no presenta alergias"
    )

    class Factory(
        private val application: Application,
        private val sharedViewModel: SharedViewModel
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(PatientDataViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return PatientDataViewModel(application, sharedViewModel) as T
            }
            throw IllegalArgumentException("Unable to construct view model")
        }
    }
}