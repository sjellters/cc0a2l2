package com.uni.cc0a2.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.uni.cc0a2.R
import com.uni.cc0a2.databinding.HomeFragmentBinding
import com.uni.cc0a2.viewmodels.HomeViewModel
import com.uni.cc0a2.viewmodels.SharedViewModel

class HomeFragment : Fragment() {

    private val viewModel: HomeViewModel by lazy {
        val sharedViewModel: SharedViewModel by activityViewModels()
        val activity = requireNotNull(this.activity) {
            "You can only access the viewModel after onActivityCreated()"
        }

        ViewModelProvider(
            this,
            HomeViewModel.Factory(activity.application, sharedViewModel)
        ).get(HomeViewModel::class.java)
    }

    private lateinit var binding: HomeFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.home_fragment, container, false
        )

        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter =
            ArrayAdapter(requireContext(), R.layout.document_type_item, viewModel.documentTypes)

        binding.documentTypeTextField.setAdapter(adapter)
        binding.searchPatientButton.setOnClickListener {
            val action = HomeFragmentDirections.actionHomeFragmentToPatientDataFragment()
            it.findNavController().navigate(action)
        }
    }

}