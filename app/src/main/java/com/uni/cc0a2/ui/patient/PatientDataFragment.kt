package com.uni.cc0a2.ui.patient

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import com.uni.cc0a2.R
import com.uni.cc0a2.databinding.PatientDataFragmentBinding
import com.uni.cc0a2.viewmodels.PatientDataViewModel
import com.uni.cc0a2.viewmodels.SharedViewModel

class PatientDataFragment : Fragment() {

    private val viewModel: PatientDataViewModel by lazy {
        val sharedViewModel: SharedViewModel by activityViewModels()
        val activity = requireNotNull(this.activity) {
            "You can only access the viewModel after onActivityCreated()"
        }

        ViewModelProvider(
            this,
            PatientDataViewModel.Factory(activity.application, sharedViewModel)
        ).get(PatientDataViewModel::class.java)
    }

    private lateinit var binding: PatientDataFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.patient_data_fragment, container, false
        )

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.lifecycleOwner = this
        binding.currentPatient = viewModel.currentPatient
    }

}