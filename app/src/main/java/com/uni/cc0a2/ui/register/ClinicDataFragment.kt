package com.uni.cc0a2.ui.register

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.uni.cc0a2.R
import com.uni.cc0a2.viewmodels.ClinicDataViewModel

class ClinicDataFragment : Fragment() {

    companion object {
        fun newInstance() = ClinicDataFragment()
    }

    private lateinit var viewModel: ClinicDataViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.clinic_data_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(ClinicDataViewModel::class.java)
        // TODO: Use the ViewModel
    }

}