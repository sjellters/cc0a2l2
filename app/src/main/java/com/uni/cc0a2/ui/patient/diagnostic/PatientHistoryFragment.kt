package com.uni.cc0a2.ui.patient.diagnostic

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.uni.cc0a2.R
import com.uni.cc0a2.viewmodels.PatientHistoryViewModel

class PatientHistoryFragment : Fragment() {

    companion object {
        fun newInstance() = PatientHistoryFragment()
    }

    private lateinit var viewModel: PatientHistoryViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.patient_history_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(PatientHistoryViewModel::class.java)
        // TODO: Use the ViewModel
    }

}