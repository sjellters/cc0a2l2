package com.uni.cc0a2.ui.patient.diagnostic

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.uni.cc0a2.R
import com.uni.cc0a2.viewmodels.DiagnosticViewModel

class DiagnosticFragment : Fragment() {

    companion object {
        fun newInstance() = DiagnosticFragment()
    }

    private lateinit var viewModel: DiagnosticViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.diagnostic_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(DiagnosticViewModel::class.java)
        // TODO: Use the ViewModel
    }

}